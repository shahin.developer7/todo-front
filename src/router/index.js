import { createRouter, createWebHistory } from "vue-router";
import Login from "../views/auth/Login.vue";
import Register from "../views/auth/Register.vue";
import Home from "../views/Home.vue";

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: "/",
			name: "Home",
			// route level code-splitting
			// this generates a separate chunk (About.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			// component: () => import("../views/Home.vue"),
			component: Home,
		},
		{
			path: "/login",
			name: "Login",
			component: Login,
		},
		{
			path: "/register",
			name: "Register",
			component: Register,
		},
	],
});

router.beforeEach((to, from, next) => {
	let token = localStorage.getItem("token");
	if (token) {
		if (to.name == "Login" || to.name == "Register") next("/");
		next();
	} else {
		if (to.name == "Login" || to.name == "Register") next();
		if (to.name != "Login") next("/login");
		next();
	}
});

export default router;
