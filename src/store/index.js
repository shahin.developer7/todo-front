import { createStore } from "vuex";
import base from "@/store/modules/base";
import notification from "@/store/modules/notification";

const store = createStore({
	state: {},
	mutations: {},
	actions: {},
	modules: {
		base,
		notification,
	},
});

export default store;
