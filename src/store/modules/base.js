const base = {
	namespaced: true,
	state: () => ({
		count: 0,
	}),
	mutations: {
		increment(state, data) {
			state.count += data;
		},
	},
	actions: {
		doIncrement(context, data) {
			context.commit("increment", data);
		},
	},
};

export default base;
