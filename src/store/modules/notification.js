const notification = {
	namespaced: true,
	state: () => ({
		list: [],
	}),
	mutations: {
		add(state, item) {
			state.list = [...state.list, item];
		},
		remove(state, id) {
			state.list = state.list.map((el) => el.id != id);
		},
	},
	actions: {
		add(
			context,
			{
				text = "",
				timeout = 5000,
				type = "error", // success | error | warning
			}
		) {
			console.log("data: ", { text, timeout, type });
			context.commit("add", { text, timeout, type });
		},
		remove(context, data) {
			context.commit("remove", data);
		},
	},
};

export default notification;
