import axios from "axios";
import store from "../store";
// api collections
import Auth from "./auth";
import Task from "./task";
import router from "../router";
const API_URL = import.meta.env.VITE_BASE_URL;

export default class Api {
	instance = null;
	http;
	cache; // we have access to vuex
	services = new Map();

	constructor() {
		this.setupCache();
		this.setupAxios();
		this.setupServices();
	}

	setupAxios() {
		this.http = axios.create({
			baseURL: API_URL,
			headers: {
				Accept: "application/json",
			},
		});
		this.http.interceptors.request.use(
			async function (config) {
				let token = await localStorage.getItem("token");
				if (token) {
					config.headers.Authorization = `Bearer ${token}`;
				}
				return config;
			},
			function (error) {
				return Promise.reject(error);
			}
		);

		this.http.interceptors.response.use(
			function (response) {
				//2**
				// console.log(
				// 	`%c ${response.status} -> ${response.config.url}`,
				// 	"color: #77C146",
				// 	response
				// );
				return response;
			},
			async function (error) {
				//4** OR 5**
				let { status, config } = error.response;
				if (status == 401) {
					localStorage.removeItem("token");
					router.replace({ name: "Login" });
				}
				// console.log(
				// 	`%c ${status} -> ${config.url}`,
				// 	"color: #d64141",
				// 	error.response
				// );
				return Promise.reject(error);
			}
		);
	}

	setupCache() {
		this.cache = store;
	}

	//get an singleton instance of api class
	static getInstance() {
		if (this.instance != null) {
			return this.instance;
		} else {
			this.instance = new Api();
			return this.instance;
		}
	}

	//add services to api services map
	setupServices() {
		this.services.set("Auth", new Auth(this.http, this.cache));
		this.services.set("Task", new Task(this.http, this.cache));
	}

	//get access to each service
	authService() {
		return this.services.get("Auth");
	}
	taskService() {
		return this.services.get("Task");
	}
}
