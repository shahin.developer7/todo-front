export default class Task {
	http;
	cache;

	constructor(http, cache) {
		this.http = http;
		this.cache = cache;
	}

	async getList() {
		return await this.http.get(`tasks`);
	}

	async create(data) {
		return await this.http.post(`tasks`, data);
	}

	async update(taskId, data) {
		return await this.http.put(`tasks/${taskId}`, data);
	}

	async delete(taskId) {
		return await this.http.delete(`tasks/${taskId}`);
	}
}
