export default class Auth {
	http;
	cache;

	constructor(http, cache) {
		this.http = http;
		this.cache = cache;
	}
	async login(data) {
		return await this.http.post(`login`, data);
	}

	async register(data) {
		return await this.http.post(`register`, data);
	}

	async logout() {
		return await this.http.post(`logout`);
	}
}
